import {createRouter, createWebHistory} from 'vue-router'

const Login = () => import('../views/Login')
const LoginByPhone = () => import('../views/LoginByPhone')
const Layout = () => import('../layout/Layout')
const Person = () => import('../views/Person')
const Student = () => import('../views/Student')
const Admin = () => import('../views/Admin')
const Advice = () => import('../views/Advice')
const BadGoods = () => import('../views/BadGoods')
const Card = () => import('../views/Card')
const Leave = () => import('../views/Leave.vue')
const Violation = () => import('../views/Violation')

const routes = [
  {
    path: '/',
    name: 'Layout',
    component: Layout,

    children: [
      {
        path: 'person',
        name: 'Person',
        component: Person
      },
      {
        path: 'leave',
        name: 'Leave',
        component: Leave
      },
      {
        path: 'violation',
        name: 'Violation',
        component: Violation
      },
      {
        path: 'card',
        name: 'Card',
        component: Card
      },
      {
        path: 'badgoods',
        name: 'BadGoods',
        component: BadGoods
      },
      {
        path: 'advice',
        name: 'Advice',
        component: Advice
      },
      {
        path: 'student',
        name: 'Student',
        component: Student
      },
      {
        path: 'admin',
        name: 'Admin',
        component: Admin
      },
    ],
    //路由自动跳转
    redirect: "/login"
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/loginbyphone',
    name: 'LoginByPhone',
    component: LoginByPhone
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
