import axios from 'axios'
import router from "../router";
import {ElMessage} from "element-plus";

const request = axios.create({
    baseURL: "/api",
    timeout: 5000
})


// 请求白名单，如果请求在白名单里面，将不会被拦截校验权限
const whiteUrls = ["/users/login","/users/sendCode","/users/loginByPhone","/users/getCaptcha"]

// request 拦截器
// 可以自请求发送前对请求做一些处理
request.interceptors.request.use(config => {
    config.headers['Content-Type'] = 'application/json;charset=utf-8';

    // 取出sessionStorage里面缓存的用户信息
    let userJson = sessionStorage.getItem("user")
    if (!whiteUrls.includes(config.url)) {  // 校验请求白名单
        //如果为空
        if(!userJson) {
            console.log(userJson);
            ElMessage({
                type: "error",
                message: "请先登录!"
            })
            router.push("/login")
        }

    }
    return config
}, error => {
    return Promise.reject(error)
});

// response 拦截器
// 可以在接口响应后统一处理结果
request.interceptors.response.use(
    response => {
        let res = response.data;
        // 兼容服务端返回的字符串数据
        if (typeof res === 'string') {
            res = res ? JSON.parse(res) : res
        }
        return res;
    },
    error => {
        console.log('err' + error) // for debug
        return Promise.reject(error)
    }
)

export default request

